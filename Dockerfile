FROM python:3.9-alpine3.13
LABEL maintainer="Espie Lomat"

ENV PYTHONUNBUFFERED 1
ENV DJANGO_SETTINGS_MODULE=config.settings.local

COPY ./app /app
WORKDIR /app
EXPOSE 8003

COPY Pipfile /app
COPY Pipfile.lock /app

RUN pip install --upgrade pip && \
    apk add --update --no-cache postgresql-client && \
    apk add --update --no-cache --virtual .tmp-build-deps \
        build-base postgresql-dev musl-dev && \
    apk del .tmp-build-deps && \
    adduser --disabled-password --no-create-home django-user

RUN apk add py3-pip py3-pillow py3-cffi py3-brotli gcc musl-dev python3-dev pango postgresql-dev

RUN pip install pipenv
RUN pipenv install --deploy --system --dev

RUN apk --update --upgrade --no-cache add fontconfig ttf-freefont font-noto terminus-font \
   && fc-cache -f \
   && fc-list | sort

user django-user
