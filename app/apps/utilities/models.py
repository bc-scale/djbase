from django.db import models
from django.conf import settings


class UserLogModel(models.Model):
    """ Create abstract model that will hold the user who created/modified the records """
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        on_delete=models.SET_NULL,
        related_name='%(app_label)s_%(class)s_created_by'
    )

    modified_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name='%(app_label)s_%(class)s_modified_by'
    )

    class Meta:
        abstract = True