from datetime import datetime
from django.utils import timezone
from constance import config

from django_weasyprint.utils import django_url_fetcher


def current_datetime_adjusted(interval_days=None) -> datetime:
    """ Added n days to current datetime """
    cur_date = timezone.now()
    if not interval_days:
        interval_days = config.CLIENT_EXPIRY_DAYS
    return cur_date + timezone.timedelta(days=interval_days)


def datetime_adjusted(dt_time, **kwargs) -> datetime:
    """ Adjust datetime based on given kwargs """
    return dt_time + timezone.timedelta(**kwargs)


def utc_now() -> datetime:
    """ Return UTC now """
    return datetime.now(timezone.utc)


def schedule_task():
    pass