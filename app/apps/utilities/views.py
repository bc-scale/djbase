import functools

from django_weasyprint.views import WeasyTemplateResponse

from apps.utilities.common import custom_url_fetcher


class CustomWeasyTemplateResponse(WeasyTemplateResponse):
    """ customized response class to pass a kwarg to URL fetcher """

    def get_url_fetcher(self):
        """ Override to Disable host and certificate check """
        return functools.partial(custom_url_fetcher)