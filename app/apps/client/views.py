from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from apps.client.models import ClientModel
from apps.client.serializers import ClientSerializer


class ClientViewSet(ModelViewSet):
    queryset = ClientModel.objects.all()
    serializer_class = ClientSerializer
    permission_classes = (IsAuthenticated, IsAdminUser, )
    filter_backends = (DjangoFilterBackend, )
    filterset_fields = ('name', )

    def perform_create(self, serializer):
        serializer.save(created_by=self.request.user)