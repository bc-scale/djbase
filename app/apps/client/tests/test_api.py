# all cases are not covered yet
import json
import pytest
import pydash

from typing import List
from django.urls import reverse
from rest_framework import status

from apps.client.tests.factories import ClientFactory
from apps.user.tests.factories import UserFactory
from apps.user.models import CustomUser

CLIENT_LIST_URL = reverse("client-list")
pytestmark = pytest.mark.django_db

@pytest.fixture
def create_clients(request, create_client) -> List[ClientFactory]:
    """ Call another fixture to enable parametrize fixtures """
    clients = []
    data = request.param
    for client in data:
        client = create_client(**client)
        clients.append(client)

    return clients


@pytest.fixture
def create_client(**kwargs):
    """ Fixture that creates new client """
    def _client_factory(**kwargs):
        return ClientFactory(**kwargs)

    return _client_factory


def test_unauthenticated_client_api(client) -> None:
    """ Test unauthenticated user gets 401 unauthorized status code """
    response = client.get(CLIENT_LIST_URL)
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.parametrize(
    "create_clients",
    [
        [{"name": "Yahoo"}, {"name": "Google"}],
        [{"name": "Facebook"}, {"name": "Instagram"}, {"name": "Twitter"}]
    ],
    indirect=True
)
def test_admin_login_client_api(admin_client, create_clients) -> None:
    """ Test authenticated admin user will be able to get the list of registered clients """
    clients = pydash.map_(create_clients, 'name')
    response = admin_client.get(CLIENT_LIST_URL)
    data = json.loads(json.dumps(response.data['results']))
    assert response.status_code == status.HTTP_200_OK
    assert response.data["count"] == len(create_clients)
    assert all(item.get("name") in clients for item in data)


def test_member_login_client_api(client) -> None:
    """ Test authenticated regular user member will get 403 forbidden status code """
    member_user = UserFactory(category=CustomUser.Categories.MEMBER)
    client.force_login(member_user)
    response = client.get(CLIENT_LIST_URL)
    assert response.status_code == status.HTTP_403_FORBIDDEN
