import factory

from factory.django import DjangoModelFactory

from apps.client.models import ClientModel


class ClientFactory(DjangoModelFactory):
    class Meta:
        model = ClientModel

    name = factory.Sequence(lambda n: 'Test Client{}'.format(n))