from django.utils import timezone
from django_extensions.db.models import TimeStampedModel
from django.db import models

from apps.client.utilities import client_logo_directory_path
from apps.utilities.common import current_datetime_adjusted
from apps.utilities.models import UserLogModel


class ClientModel(UserLogModel, TimeStampedModel):
    """ Create Client Model Class"""

    name = models.CharField(max_length=255)
    start = models.DateTimeField(default=timezone.now)
    expiry = models.DateTimeField(default=current_datetime_adjusted)
    logo = models.FileField(
        upload_to=client_logo_directory_path,
        max_length=500,
        null=True
    )

    def __str__(self):
        return self.name

    class Meta:
        ordering = ("-id", )
