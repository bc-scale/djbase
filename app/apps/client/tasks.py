import logging

from celery import shared_task
from apps.client.models import ClientModel


@shared_task
def send_client_registration_notification(client_pk):
    """ Send Welcome Email Notification to the client """
    client = ClientModel.objects.get(id=client_pk)
    # send this message via email
    print(f"Welcome {client.name}, You have been successfully registered!!!!")