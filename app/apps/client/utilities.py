from django.utils.crypto import get_random_string


def client_logo_directory_path(instance, filename) -> str:
    """ Set directory path for client's logo """
    random_str = get_random_string(length=12)

    return f'client/logos/{random_str}/{filename}'
