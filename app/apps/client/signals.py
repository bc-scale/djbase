from django.db.models.signals import post_save
from django.dispatch import receiver
from apps.client.models import ClientModel
from apps.client.tasks import send_client_registration_notification


@receiver(post_save, sender=ClientModel)
def client_registration(sender, **kwargs):
    """ Executes task when new client has been registered in the system """
    instance = kwargs.get('instance', None)
    send_client_registration_notification.apply_async(
        args=(instance.pk, )
    )