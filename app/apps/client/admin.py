from django.contrib import admin

from apps.client.models import ClientModel

admin.site.register(ClientModel)
