from datetime import timedelta

from rest_framework import serializers

from apps.client.models import ClientModel


class ClientSerializer(serializers.ModelSerializer):

    class Meta:
        model = ClientModel
        fields = (
            'id',
            'name',
            'logo',
            'start',
            'expiry',
            'created_by',
            'modified_by',
            'created',
            'modified'
        )