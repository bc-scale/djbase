from celery import shared_task

from apps.user.models import CustomUser
from apps.user.email import SetPasswordCompletedEmailNotification


@shared_task
def set_password_completed_notification_task(user_pk: int):
    """ Task that sends Set Password Completed to all Admin Staff user """
    user = CustomUser.objects.get(pk=user_pk)
    if user.category == CustomUser.Categories.ADMIN and user.is_staff:
        return

    # retrieve admin emails
    admin = CustomUser.objects.filter(
        is_staff=True,
        category=CustomUser.Categories.ADMIN
    ).values_list(
        'email', flat=True
    )
    recipients = list(admin)
    if not recipients:
        return

    context = {'user': user}
    for recipient in recipients:
        SetPasswordCompletedEmailNotification(context=context).send([recipient])
