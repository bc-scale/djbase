import factory

from factory.django import DjangoModelFactory

from apps.user.models import CustomUser


class UserFactory(DjangoModelFactory):
    class Meta:
        model = CustomUser

    category = CustomUser.Categories.ADMIN
    email = factory.Sequence(lambda n: 'test+{}@test.com'.format(n))
    password = 'secret'

