from django.views.generic import DetailView
from django_weasyprint import WeasyTemplateResponseMixin
from djoser.views import UserViewSet
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework import status
from django.conf import settings

from apps.user.email import SetPasswordEmailNotification
from apps.user.models import CustomUser
from apps.user.tasks import set_password_completed_notification_task


class CustomUserViewSet(UserViewSet):
    @action(["post"], detail=False)
    def set_password(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.user
        user.is_active = True
        user.set_password(serializer.data["new_password"])
        user.save()

        # send email notification to admin using task
        if not user.is_staff and user.category != CustomUser.Categories.ADMIN:
            set_password_completed_notification_task.apply_async(
                args=(user.pk,)
            )
        return Response(status=status.HTTP_204_NO_CONTENT)

    def perform_create(self, serializer):
        """
        Rewrite this method to send set password email notification
        instead of activation/confirmation
        """
        user = serializer.save()
        context = {"user": user}
        SetPasswordEmailNotification(self.request, context).send([user.email])


class UserDetailView(DetailView):
    """ Create User Detail View """
    template_name = 'reports/user_details.html'
    queryset = CustomUser.objects.all()


class UserDetailPDFView(WeasyTemplateResponseMixin, UserDetailView):
    """ Create a viewable PDF File """
    pdf_stylesheets = [
        settings.STATIC_ROOT + '/css/styles.css',
    ]


class UserDetailPDFDownloadView(WeasyTemplateResponseMixin, UserDetailView):
    """ Create a downloadable PDF File """
    pdf_stylesheets = [
        settings.STATIC_ROOT + '/css/styles.css',
    ]

    def get_pdf_filename(self):
        """ set PDF file name using first name and last name """
        obj = self.get_object()
        return f'user_detail_{obj.first_name}_{obj.last_name}.pdf'
