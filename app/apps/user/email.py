from django.contrib.auth.tokens import default_token_generator
from templated_mail.mail import BaseEmailMessage

from djoser import utils
from django.conf import settings


class SetPasswordEmailNotification(BaseEmailMessage):
    template_name = "email/set_password.html"

    def get_context_data(self):
        context = super().get_context_data()

        user = context.get("user")
        context["uid"] = utils.encode_uid(user.pk)
        context["token"] = default_token_generator.make_token(user)
        context["url"] = settings.DJOSER.get('SET_PASSWORD_URL').format(**context)
        return context


class SetPasswordCompletedEmailNotification(BaseEmailMessage):
    template_name = "email/set_password_completed.html"
