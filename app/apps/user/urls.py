from django.contrib.auth import get_user_model
from rest_framework.routers import DefaultRouter

from django.urls import re_path

from apps.user.views import CustomUserViewSet, UserDetailPDFView

router = DefaultRouter()
router.register("", CustomUserViewSet)

User = get_user_model()

urlpatterns = router.urls

urlpatterns += [
    re_path(r'^report/(?P<pk>[1-9]+)/download/$', UserDetailPDFView.as_view())
]
