from rest_framework import serializers
from djoser.serializers import (
    PasswordSerializer, UserSerializer, UidAndTokenSerializer
)

from apps.user.models import CustomUser


class CustomUserSerializer(UserSerializer):

    class Meta:
        model = CustomUser
        fields = (
            'id',
            'email',
            'first_name',
            'last_name',
            'is_staff',
            'is_active',
            'category',
        )


class CustomUserCreateSerializer(serializers.ModelSerializer):
    """ Create serializer for Register New User """

    class Meta:
        model = CustomUser
        fields = (
            'email',
            'first_name',
            'last_name',
            'is_staff',
            'is_active',
            'category',
        )


class CustomSetPasswordSerializer(UidAndTokenSerializer, PasswordSerializer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["re_password"] = serializers.CharField(
            style={"input_type": "password"}
        )
