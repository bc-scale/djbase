from .common import *

ALLOWED_HOSTS = env.list('DJANGO_ALLOWED_HOSTS', default=['127.0.0.1', 'localhost'])
MIDDLEWARE = MIDDLEWARE_CLASSES
INSTALLED_APPS = INSTALLED_APPS + ['drf_spectacular']

AUTH_PASSWORD_VALIDATORS = []

# Setting for Spectacular Docs
SPECTACULAR_SETTINGS = {
    'TITLE': 'MY TEST API',
    'DESCRIPTION': 'MY API',
    'VERSION': '1.0.0',
    'SERVE_INCLUDE_SCHEMA': False,
    'COMPONENT_SPLIT_REQUEST': True
}