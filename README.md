How to setup instance?
1. Setup .env file
> cp app/.env-example app/.env
2. Have docker and docker-compose installed in your local   
> docker build .

> docker-compose up -d
3. Run migrations
> docker-compose run --rm app python manage.py makemigrations

> docker-compose run --rm app python manage.py migrate
4. Collectstatic
> docker-compose run --rm app python manage.py collectstatic
5. create superuser account
> docker-compose run --rm app python manage.py createsuperuser
6. Run Test Cases
> docker-compose run --rm app pytest -s
7. Run Bandit to check vulnerability of your code
> docker-compose run --rm app bandit -r apps/ --configfile bandit.yml --ini .bandit
